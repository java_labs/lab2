Description
-----------
Empoyee is a bstract class that have the name and year experience for each employee the experience is used to detect the cost or salary of employees based on 
thier experience

There are 2 classes that extends from this Employee:
HourEmployee
MonthlyEmployee

HourEmployee
------------
Hour employee have hours attribute,it's the number of hours on which the employee works in a day.
to calculate the salary of it you should multiple the number of hours to the cost of one hour and note that cost of one hour is based on the employee experience.

MonthlyEmployee
---------------
Monthly employee don't have more attribute than the Employee class and get salary each month to calculate the salary of it you only need  to know the month salary of 
the employee based on the experience.

Salaries enum
-------------
Salaries enum have :

MONTHLY_SALARY constant
-----------------------
It's a constant used to get the salary of the employee based on its experience

HOUR_SALARY constant
--------------------
It's a constant used to get the cost of one hour based on the employee's experience

monthlySalaryExperience map
---------------------------
It's a map used to store the experience and its corresponding salary

hourCostExperience
------------------
It's a map used to store the experience of the employee and its corresponding cost of one hour

Requirements
------------
You are require to write the code in getSalary override method that exist in both classes HourEmployees , MonthlyEmployees based on the logic to get the salary of the 2
types of the employees as we mentioned above and you must make the tests run successfully.

Note if you want to run tests from the template using the mvn command mvn test
